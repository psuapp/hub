# Docker Hub

[![pipeline status](https://gitlab.com/psuapp/hub/badges/master/pipeline.svg)](https://gitlab.com/psuapp/hub/commits/master)

This repository build Docker images for [PSU App](https://gitlab.com/psuapp) projects.

And store them to its integrated [Docker Container Registry](https://gitlab.com/psuapp/hub/container_registry).
