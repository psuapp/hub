# This file should be save here: /usr/local/apache2/conf/extra/httpd-vhosts.conf

<VirtualHost *:5000>
  # Admin email, Server Name (domain name) and any aliases
  # ServerAdmin webmaster@example.com
  # ServerName my-app.cluster.nip.io
  # ServerAlias my-app.example.com

  # Index file and Document Root (where the public files are located)
  DirectoryIndex index.php index.html

  # For Laravel
  DocumentRoot /var/www/html/public

  <Directory /var/www/html/public>
    AllowOverride all
    Require all granted
    # Require ip 127.0.0.1
  </Directory>

  <Directory /var/www/html/public/storage>
    # For uploaded files
    AllowOverride None
  </Directory>
  # End of for Laravel

  <IfModule mod_expires.c>
    <IfModule mod_headers.c>
      # Force Internet Explorer to use its last engine
      Header set X-UA-Compatible "IE=edge"

      # For Laravel
      # The Expires* directives requires the Apache module
      # `mod_expires` to be enabled.
      <Location ~ "/build/">
        # Use of ETag is discouraged when Last-Modified is present
        Header unset ETag
        FileETag None
        # RFC says only cache for 1 year
        ExpiresActive On
        ExpiresDefault "access plus 1 year"
      </Location>
      # End of for Laravel
    </IfModule>
  </IfModule>

  # Enable http authorization headers
  <IfModule setenvif_module>
      SetEnvIfNoCase ^Authorization$ "(.+)" HTTP_AUTHORIZATION=$1
  </IfModule>
  <FilesMatch ".+\.ph(p[3457]?|t|tml)$">
      SetHandler "proxy:unix:/var/run/php/php-fpm.sock|fcgi://localhost"
  </FilesMatch>
  <FilesMatch ".+\.phps$">
      # Deny access to raw php sources by default
      # To re-enable it's recommended to enable access to the files
      # only in specific virtual host or directory
      Require all denied
  </FilesMatch>
  # Deny access to files without filename (e.g. '.php')
  <FilesMatch "^\.ph(p[3457]?|t|tml|ps)$">
      Require all denied
  </FilesMatch>
  # <LocationMatch "/(build|fonts|storage)">
  #     # Disable PHP execution on public assets and file uploads directories
  #     SetHandler None
  # </LocationMatch>
  <LocationMatch "/(build|storage)/.*\.ph(p[3457]?|t|tml)">
      # For Laravel
      # Deny access to PHP files inside assets and file upload directories
      Require all denied
  </LocationMatch>

  # Custom log file locations
  LogLevel warn
  # ErrorLog ${APACHE_LOG_DIR}/error.log
  # CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
